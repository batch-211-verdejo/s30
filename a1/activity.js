// Total number of fruits on sale
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$count: "fruitsOnSale"}
]);

// Total number of fruits with stock more than 20
db.fruits.aggregate([
    {$match: {stock: {$gte: 20}}},
    {$count: "enoughStock"}
]);

// Average price of fruits onSale per supplier
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);

// Highest price of a fruit per supplier
db.fruits.aggregate([    
    {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
]);

// Lowest price of a fruit per supplier
db.fruits.aggregate([    
    {$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
]);